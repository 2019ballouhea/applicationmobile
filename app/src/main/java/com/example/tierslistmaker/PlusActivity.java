package com.example.tierslistmaker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;


public class PlusActivity extends AppCompatActivity {

    int dernierID;

    SharedPreferences sp;

    TextView criterion1;
    TextView criterion2;
    TextView criterion3;
    TextView criterion4;
    TextView criterion5;

    RatingBar rBMiseEnScene;
    RatingBar rBRolesP;
    RatingBar rBRolesS;
    RatingBar rBDecors;
    RatingBar rBWaouh;

    TextView tVMiseEnScene;
    TextView tVRolesP;
    TextView tVRolesS;
    TextView tVDecors;
    TextView tVWaouh;

    float MiseEnScene =-1;
    float RolesP=-1;
    float RolesS=-1;
    float Decors=-1;
    float Waouh=-1;
    float note;

    String PlayName;
    String TheaterName;
    String ProducerName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plus);

        //On fait afficher les notes en-dessous de chaque RatingBar
        tVMiseEnScene = findViewById(R.id.tVMiseEnScene);
        tVRolesP = findViewById(R.id.tVRolesP);
        tVRolesS =findViewById(R.id.tVRolesS);
        tVDecors = findViewById(R.id.tVDecors);
        tVWaouh = findViewById(R.id.tVWaouh);

        rBMiseEnScene = findViewById(R.id.rBMiseEnScene);
        rBMiseEnScene.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                setText(rBMiseEnScene,tVMiseEnScene);
            }
        });

        rBRolesP = findViewById(R.id.rBRolesP);
        rBRolesP.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                setText(rBRolesP,tVRolesP);
            }
        });

        rBRolesS =findViewById(R.id.rBRolesS);
        rBRolesS.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                setText(rBRolesS,tVRolesS);
            }
        });

        rBDecors = findViewById(R.id.rBDecors);
        rBDecors.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                setText(rBDecors,tVDecors);
            }
        });

        rBWaouh = findViewById(R.id.rBWaouh);
        rBWaouh.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                setText(rBWaouh,tVWaouh);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        criterion1=findViewById(R.id.criterion1);
        criterion2=findViewById(R.id.criterion2);
        criterion3=findViewById(R.id.criterion3);
        criterion4=findViewById(R.id.criterion4);
        criterion5=findViewById(R.id.criterion5);

        //On récupère les critères personnalisés s'ils existent
        sp=getSharedPreferences("param", Context.MODE_PRIVATE);
        if (sp.contains("criterion1") && sp.getString("criterion1","")!=""){
            criterion1.setText(sp.getString("criterion1",""));
        }
        else{
            criterion1.setText("Intelligence de la mise en scène");
        }
        if (sp.contains("criterion2") && sp.getString("criterion2","")!=""){
            criterion2.setText(sp.getString("criterion2",""));
        }
        else{
            criterion2.setText("Justesse des rôles principaux");
        }
        if (sp.contains("criterion3") && sp.getString("criterion3","")!=""){
            criterion3.setText(sp.getString("criterion3",""));
        }
        else{
            criterion3.setText("Justesse des rôles secondaires");
        }
        if (sp.contains("criterion4")&& sp.getString("criterio4","")!=""){
            criterion4.setText(sp.getString("criterion4",""));
        }
        else{
            criterion4.setText("Qualité des décors");
        }
        if (sp.contains("criterion5")&& sp.getString("criterion5","")!=""){
            criterion5.setText(sp.getString("criterion5",""));
        }
        else{
            criterion5.setText("Effet Waouh");
        }


        //On récupère l'ID,ie la clée primaire de la nouvelle entité
        sp=getSharedPreferences("res", Context.MODE_PRIVATE);
        if (sp.contains("dernierID")){
            dernierID =sp.getInt("dernierID",1);
        }
        else {
            dernierID =1;
        }

        //On récupère les champs rentrés dans IniatePlusActivity
        Intent data = getIntent();
        PlayName = data.getStringExtra("PlayName");
        ProducerName = data.getStringExtra("ProducerName");
        TheaterName = data.getStringExtra("TheaterName");


    }

    //La fonction est triviale, mais permettrait d'afficher un message différent selon la valeur rentrée dans la RatingBar
    public void setText( RatingBar RB, TextView tv){
        tv.setText("" + RB.getRating());
    }

    public void onClick(View view) {
        //On rentre les notes et informations récupérées dans la base de données
        RolesP = rBRolesP.getRating();
        RolesS = rBRolesS.getRating();
        Decors = rBDecors.getRating();
        Waouh  = rBWaouh.getRating();
        MiseEnScene = rBMiseEnScene.getRating();
        note = (RolesP+RolesS+Decors+Waouh+MiseEnScene)/5;

        sp=getSharedPreferences("res", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();

        FichePieceDatabaseOperations fpdo = new FichePieceDatabaseOperations(this);
        fpdo.add(new FichePiece(dernierID,PlayName,ProducerName,TheaterName,note));

        //On incrément ID pour la prochaine création d'une entité
        editor.putInt("dernierID", dernierID+1);
        editor.apply();

        //On retourne au menu principal
        Intent ToActivityMain = new Intent();
        ToActivityMain.setClass(this, MainActivity.class);
        startActivity(ToActivityMain);
        finish();

    }
}