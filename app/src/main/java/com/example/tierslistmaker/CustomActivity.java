package com.example.tierslistmaker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class CustomActivity extends AppCompatActivity {

    SharedPreferences sp;

    String criterion1;
    String criterion2;
    String criterion3;
    String criterion4;
    String criterion5;

    EditText ed1;
    EditText ed2;
    EditText ed3;
    EditText ed4;
    EditText ed5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom);
        ed1=findViewById(R.id.ed1);
        ed2=findViewById(R.id.ed2);
        ed3=findViewById(R.id.ed3);
        ed4=findViewById(R.id.ed4);
        ed5=findViewById(R.id.ed5);
    }

    @Override
    protected void onStart() {
        super.onStart();
        sp=getSharedPreferences("param", Context.MODE_PRIVATE);
    }

    public void Validation(View view) {
        //Renvoie les critères nouvellement entrés vers ActivityPlus
        Intent ToMainActivity = new Intent();
        ToMainActivity.setClass(this, MainActivity.class);
        startActivity(ToMainActivity);
        SharedPreferences.Editor editor =sp.edit();

        criterion1=ed1.getText().toString();
        criterion2=ed2.getText().toString();
        criterion3=ed3.getText().toString();
        criterion4=ed4.getText().toString();
        criterion5=ed5.getText().toString();

        editor.putString("criterion1",criterion1);
        editor.putString("criterion2",criterion2);
        editor.putString("criterion3",criterion3);
        editor.putString("criterion4",criterion4);
        editor.putString("criterion5",criterion5);
        editor.apply();

        finish();
    }

    public void Annulation(View view) {
        //Revient au menu principal
        Intent ToMainActivity = new Intent();
        ToMainActivity.setClass(this, MainActivity.class);
        startActivity(ToMainActivity);
        finish();
    }
}