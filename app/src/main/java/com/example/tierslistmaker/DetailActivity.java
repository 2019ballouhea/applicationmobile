package com.example.tierslistmaker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.List;

public class DetailActivity extends AppCompatActivity {

    private FichePieceDatabaseOperations udo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        udo = new FichePieceDatabaseOperations(this);
        setContentView(R.layout.activity_detail);
        List<FichePiece> ListPieces = udo.getAll();
        int dernierID = ListPieces.size();
        Intent data = getIntent();
        String IDPiece = data.getStringExtra("IDPiece");

        for (int i = 0; i < dernierID; i++) {
        //On regarde toutes les pièces de la base de données
            if ((""+ListPieces.get(i).getId()).equals(""+IDPiece)){
            //Lorsque l'ID de la pièce correspond, on affiche les attributs de l'entité
                TextView Titre = findViewById(R.id.NomPiece);
                Titre.setText("Titre de la pièce : "+ ListPieces.get(i).getPiece());
                TextView Theatre = findViewById(R.id.NomTheatre);
                Theatre.setText("Nom du théatre : "+ ListPieces.get(i).getTheatre());
                TextView MetteurEnScene = findViewById(R.id.NomMetteurEnScene);
                MetteurEnScene.setText("Metteur en scène : "+ ListPieces.get(i).getMetteur_en_scene());
                TextView Note = findViewById(R.id.Note);
                Note.setText("Note : "+ ListPieces.get(i).getNote());
            }
        }
    }

}