package com.example.tierslistmaker;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "FichePiece")
public class FichePiece {

    @ColumnInfo(name = "ID")
    @NonNull
    @PrimaryKey
    public int ID;

    @ColumnInfo(name = "Pièce")
    @NonNull
    private String piece;

    @ColumnInfo(name = "Metteur en scène")
    @NonNull
    private String metteur_en_scene;

    @ColumnInfo(name = "Théâtre")
    @NonNull
    private String theatre;

    @ColumnInfo(name = "Note")
    @NonNull
    private float note;





    public FichePiece(int ID, String piece, String metteur_en_scene, String theatre,float note) {
        this.ID = ID;
        this.piece = piece;
        this.metteur_en_scene=metteur_en_scene;
        this.theatre= theatre;
        this.note=note;
    }

    public int getId() {
        return ID;
    }
    public String getPiece(){return piece;}
    public String getMetteur_en_scene(){return metteur_en_scene;}
    public String getTheatre(){return theatre;}
    public float getNote(){return note;}
}