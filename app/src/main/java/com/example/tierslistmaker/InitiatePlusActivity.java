package com.example.tierslistmaker;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class InitiatePlusActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initiate_plus);

    }
    public void LaunchActivityPlus2(View view) {
    //Récupère le contenu des champs texte et les renvoie vers ActivityPlus
        EditText PlayName = (EditText)findViewById(R.id.editPlayName);
        EditText ProducerName = (EditText)findViewById(R.id.editProducer);
        EditText TheaterName = (EditText)findViewById(R.id.editTheater);


        Intent ToActivityPlus = new Intent();
        ToActivityPlus.setClass(this, PlusActivity.class);

        ToActivityPlus.putExtra("PlayName", PlayName.getText().toString());
        ToActivityPlus.putExtra("ProducerName",ProducerName.getText().toString());
        ToActivityPlus.putExtra("TheaterName",TheaterName.getText().toString());

        startActivity(ToActivityPlus);
    }
}