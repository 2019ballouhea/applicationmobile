package com.example.tierslistmaker;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private FichePieceDatabaseOperations udo;
    int dernierID;
    SharedPreferences sp;
    TextView tv;


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        udo = new FichePieceDatabaseOperations(this);

        // On affiche les pièces enregistrées dans la base de données
        display(this);

    }


    @Override
    protected void onStart() {
        super.onStart();

        // On affiche un message si et seulement si aucune TiersListe n'est encore créée
        List<FichePiece> ListPieces = udo.getAll();
        dernierID = ListPieces.size();
        tv = findViewById(R.id.textView);
        if (dernierID !=0){
            tv.setVisibility(View.INVISIBLE);
        }
        else{
            tv.setVisibility(View.VISIBLE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void display(Context context){

        LinearLayout linearLayout = findViewById(R.id.LinLay);

        //lance une activité informative, correspondant au bouton cliqué
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button)v;
                LaunchDetailActivity(v, b.getTag().toString());
            }
        };
        List<FichePiece> ListPieces = udo.getAll();
        String buttonName;
        String buttonTag;
        for (int i = 0; i < ListPieces.size(); i++) {
            buttonName = ListPieces.get(i).getPiece();
            buttonTag = ""+ ListPieces.get(i).getId();
            Button button = new Button(context);
            button.setText(buttonName);
            button.setTag(buttonTag);
            button.setId(View.generateViewId());
            button.setOnClickListener(onClickListener);
            linearLayout.addView(button);

        }
    }

    public void LaunchActivityPlus1(View view) {
        //Lance la saisie d'une nouvelle entité
        Intent ToInitiateActivityPlus = new Intent();
        ToInitiateActivityPlus.setClass(this, InitiatePlusActivity.class);
        startActivity(ToInitiateActivityPlus);
    }

    public void LaunchDetailActivity(View view, String IDPiece) {
        //Lance une activité donnant des informations sur la pièce associée à cet ID
        Intent ToDetailActivity = new Intent();
        ToDetailActivity.setClass(this, DetailActivity.class);
        ToDetailActivity.putExtra("IDPiece", IDPiece);
        startActivity(ToDetailActivity);
    }

    public void deleteAll(View view) {
        //Efface la TiersList
        LinearLayout linearLayout = findViewById(R.id.LinLay);
        linearLayout.removeAllViews();
        udo.deleteAll();
        sp =getSharedPreferences("res", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor =sp.edit();
        editor.putInt("dernierID",1);
        editor.apply();
        tv.setVisibility(View.VISIBLE);

    }

    public void customize(View view) {
        //Lance une activité de personnalisation des critères
        Intent ToCustomActivity = new Intent();
        ToCustomActivity.setClass(this, CustomActivity.class);
        startActivity(ToCustomActivity);
    }
}