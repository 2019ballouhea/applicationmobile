package com.example.tierslistmaker;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface FichePieceDAO {

    @Query("DELETE FROM FichePiece")
    void deleteAll();

    @Insert
    void ajouter(FichePiece fichePièce);

    @Query("SELECT * FROM FichePiece WHERE ID = :ID LIMIT 1")
    FichePiece getID(int ID);

    @Query("SELECT * FROM FichePiece ORDER BY note DESC")
    List<FichePiece> getAll();


}