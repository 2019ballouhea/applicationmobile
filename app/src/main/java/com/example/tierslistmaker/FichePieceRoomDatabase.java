package com.example.tierslistmaker;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {FichePiece.class}, version=1, exportSchema = false)
public abstract class FichePieceRoomDatabase extends RoomDatabase {

    public abstract FichePieceDAO fichePieceDAO();

    static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(4);

    // patron singleton
    private static volatile FichePieceRoomDatabase INSTANCE;

    static FichePieceRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (FichePieceRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            FichePieceRoomDatabase.class, "fichepiece.bd")
                            .addCallback(roomDatabaseCallback).build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback roomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen (@NonNull SupportSQLiteDatabase db){
            super.onOpen(db);

            databaseWriteExecutor.execute(() -> {
            });
        }
    };
}
