package com.example.tierslistmaker;

import android.content.Context;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class FichePieceDatabaseOperations {

    private FichePieceRoomDatabase fichePieceRoomDatabase;
    private FichePieceDAO fichePieceDAO;

    public FichePieceDatabaseOperations(Context context) {
        fichePieceRoomDatabase = FichePieceRoomDatabase.getDatabase(context);
        fichePieceDAO = fichePieceRoomDatabase.fichePieceDAO();
    }

    //Ajoute une entité dans la base de données
    public void add(FichePiece u) {
        FichePieceRoomDatabase.databaseWriteExecutor.execute(() -> {
            fichePieceDAO.ajouter(u);
        });
    }

    //Renvoie tout le contenu de la base de données, ordonné par notes décroissantes
    public List<FichePiece> getAll() throws FichePieceInconnueException {
        Future<List<FichePiece>> resultat = FichePieceRoomDatabase.databaseWriteExecutor.submit(new Callable<List<FichePiece>>() {
            @Override
            public List<FichePiece> call() throws Exception {
                List<FichePiece> u = fichePieceDAO.getAll();
                if (u != null) {
                    return u;
                } else {
                    throw new FichePieceInconnueException("La table est vide.");
                }
            }
        });
        try {
            return resultat.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        throw new FichePieceInconnueException("La tables est vide.");

    }

    //Efface la base de données
    public void deleteAll() {
        FichePieceRoomDatabase.databaseWriteExecutor.execute(() -> {
            fichePieceDAO.deleteAll();
        });
    }
}


