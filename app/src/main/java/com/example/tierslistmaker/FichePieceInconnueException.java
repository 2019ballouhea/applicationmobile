package com.example.tierslistmaker;

class FichePieceInconnueException extends RuntimeException {
    public FichePieceInconnueException(String message) {
        super(message);
    }
}